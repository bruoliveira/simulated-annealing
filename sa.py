#coding=utf-8
import sys  
reload(sys)  
sys.setdefaultencoding('utf8')

import linecache
from random import randint
from copy import deepcopy
from math import exp, cos, radians
import random as rnd
import matplotlib.pyplot as plt
from timeit import default_timer as timer
import json


class SA(object):

	def __init__(self, arq_variaveis):
		super(SA, self).__init__()
		self.arq_variaveis = arq_variaveis
		self.variaveis = []
		self.clausulas = []
		self.proximo = []
		self.arq_variaveis = arq_variaveis

		arquivo = open(arq_variaveis, 'r')
		linhas = arquivo.readlines()
		info = linhas[7].split()
		self.num_variaveis = int(info[2])
		self.variaveis = self.gerar_solucao()
		
		for i in range(8, int(info[3])+8):
			clausula = linhas[i].split()
			self.clausulas.append(clausula)

	def gerar_solucao(self):
		solucao = []
		for i in range(self.num_variaveis):
			bit = randint(0,1)
			if bit == 0:
				solucao.append(False)
			else:
				solucao.append(True)
		return solucao

	def avaliar_clausulas(self, variaveis):
		clausulas_validas = 0
		for clausula in self.clausulas:
			var1 = variaveis[abs(int(clausula[0]))-1]
			var2 = variaveis[abs(int(clausula[1]))-1]
			var3 = variaveis[abs(int(clausula[2]))-1]

			#negar as variaveis que devem ser negadas na clausula
			if int(clausula[0]) < 0:
				var1 = not var1
			if int(clausula[1]) < 0:
				var2 = not var2
			if int(clausula[2]) < 0:
				var3 = not var3

			if var1 or var2 or var3:
				clausulas_validas += 1
		return clausulas_validas

	#resfriamento linear (Cooling Schedule 0)
	def resf_0(self, t0, tn, n, i):
		return float(t0) - float(i*(t0-tn))/float(n)

	#resfriamento suavizado (Cooling Schedule 1)
	def resf_1(self, t0, tn, n, i):
		return float(t0) * float((tn/t0)**float(i/float(n)))

	#resfriamento abrupto (Cooling Schedule 2)
	def resf_2(self, t0, tn, n, i):
		a = float(((t0-tn)*(n+1)))/float(n)
		b = t0-a
		return float(a/(i+1)) + b

	#resfriamento cosseno (Cooling Schedule 6)
	def resf_3(self, t0, tn, n, i):
		return 1/float(2) * float(t0-tn) * float(1+cos(i*3.145/float(n))) + float(tn)

	def diminuir_temperatura(self, t0, tn, n, i, func_resf):
		if func_resf == 0:
			return self.resf_0(t0, tn, n, i)
		elif func_resf == 1:
			return self.resf_1(t0, tn, n, i)
		elif func_resf == 2:
			return self.resf_2(t0, tn, n, i)
		elif func_resf == 3:
			return self.resf_3(t0, tn, n, i)

	def perturbacao(self):
		proximo = deepcopy(self.variaveis)
		pos = rnd.randint(0, len(self.variaveis)-1)
		proximo[pos] = not proximo[pos]
		return proximo

	def recozimento(self, t0, tn, n, func_resf, exe):
		tempo_inicial = timer()
		energia = self.avaliar_clausulas(self.variaveis)
		hist_energia = [energia]
		t = t0
		hist_temperatura = [t]
		iteracoes = 1
		maior_energia = 0

		while (iteracoes < n):
			self.proximo = self.perturbacao()
			energia_proximo = self.avaliar_clausulas(self.proximo)
			delta = energia_proximo - energia
			if energia_proximo > maior_energia:
				maior_energia = energia_proximo
				melhor_solucao = deepcopy(self.proximo)
			if delta >= 0:
				self.variaveis = self.proximo
				energia = energia_proximo
			else:
				if t > 0.1:
					probabilidade = exp(delta/float(t))
				else:
					probabilidade = exp(delta/0.1)
				if rnd.uniform(0,1) < probabilidade:
					self.variaveis = self.proximo
					energia = energia_proximo
			t = self.diminuir_temperatura(t0, tn, n, iteracoes, func_resf)
			hist_temperatura.append(t)
			hist_energia.append(energia)
			iteracoes += 1
		tempo_final = timer()
		fig = plt.figure()
		plt.title('Simulated Annealing: %s' % self.arq_variaveis)
		plt.xlim([0,n])
		plt.ylim([0,len(self.clausulas)+1])
		plt.xlabel('Iterações')
		plt.ylabel('Energia', color='b')
		plt.tick_params(axis="y", labelcolor="b")
		plt.plot(hist_energia, "b-", linewidth=1)

		plt.twinx()
		plt.xlim([0,n])
		plt.ylim([0,t0+1])
		plt.ylabel('Temperatura', color='r')
		plt.tick_params(axis="y", labelcolor="r")
		plt.plot(hist_temperatura, "r-", linewidth=1)
		#plt.show()
		
		fig.savefig('gráficos/SA_%s_%d.pdf' % (self.arq_variaveis, exe))

		resultados = {}
		resultados['tempo'] = tempo_final - tempo_inicial
		resultados['energia_final'] = energia
		resultados['solucao_final'] = self.variaveis
		resultados['melhor_energia'] = maior_energia
		resultados['melhor_solucao'] = melhor_solucao

		with open("resultados/%s_SA_results.json" % self.arq_variaveis, 'a') as arq_resultado:
			json.dump(resultados, arq_resultado)
		arq_resultado.close()

		return hist_energia, len(self.clausulas)+1, hist_temperatura, maior_energia

	def busca_aleatoria(self, n, exe):
		tempo_inicial = timer()
		energia = self.avaliar_clausulas(self.variaveis)
		hist_energia = [energia]
		iteracoes = 1
		maior_energia = 0

		while iteracoes < n:
			self.variaveis = self.gerar_solucao()
			energia = self.avaliar_clausulas(self.variaveis)
			hist_energia.append(energia)
			if energia > maior_energia:
				maior_energia = energia
				melhor_solucao = deepcopy(self.variaveis)
			iteracoes += 1
		tempo_final = timer()
		fig = plt.figure()
		plt.title('Busca Aleatória: %s' % self.arq_variaveis)
		plt.xlim([0,n])
		plt.ylim([0,len(self.clausulas)+1])
		plt.xlabel('Iterações')
		plt.ylabel('Energia', color='b')
		plt.tick_params(axis="y", labelcolor="b")
		plt.plot(hist_energia, "b-", linewidth=1)
		fig.savefig('gráficos/BA_%s_%d.pdf' % (self.arq_variaveis, exe))
		

		resultados = {}
		resultados['tempo'] = tempo_final - tempo_inicial
		resultados['energia_final'] = energia
		resultados['solucao_final'] = self.variaveis
		resultados['melhor_energia'] = maior_energia
		resultados['melhor_solucao'] = melhor_solucao

		with open("resultados/%s_BA_results.json" % self.arq_variaveis, 'a') as arq_resultado:
			json.dump(resultados, arq_resultado)
		arq_resultado.close()

		return hist_energia, len(self.clausulas)+1, maior_energia