# Simulated Annealing e Busca Aleatória

Este algoritmo foi implementado para a disciplina de Inteligência Artificial do Curso de Ciência da Computação - UDESC para resolver problemas 3-CNF-SAT.

Os testes foram executados com as seguintes instâncias:

* uf20-01.cnf - 20 variáveis, 91 clásulas
* uf100-01.cnf - 100 variáveis, 430 clásulas 
* uf250-01.cnf - 250 variáveis, 1065 clásulas

As instâncias foram retiradas do site [SATLIB](http://www.cs.ubc.ca/~hoos/SATLIB/benchm.html).

##Para executar:

```
python main.py -a <algoritmo> -i <arquivo_entrada> -ti <temp_inicial> -tf <temp_final> -it <iterações> -r <0|1|2|3> -e <numero_execuções>
```

###Argumentos Obrigatórios:
```
-i INPUT			Caminho para o arquivo de entrada
-a ALG				Algoritmo a ser utilizado no processamento. SA = Simulated Annealing, BA = Busca Aleatória
-it ITERACOES		Número de iterações (avaliações de função) que serão realizadas
-e EXECUCOES		Número de execuções do algoritmo
```

###Somente para o Simulated Annealing:
```
-ti T_INICIAL 		Temperatura inicial
-tf T_FINAL 		Temperatura final. Deve ser diferente de 0
-r RESFRIAMENTO 	Função de resfriamento: 0|1|2|3
```
####Funções de Resfriamento

![Funções de Resfriamento](resfriamento.png)